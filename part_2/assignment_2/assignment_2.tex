\documentclass[12pt]{article}

\usepackage{times}
\usepackage[margin=0.75in]{geometry}
\usepackage{amsmath}
\usepackage{graphicx}
\setlength{\parindent}{0em}
\setlength{\parskip}{1em}
\usepackage{hyperref}

\begin{document}

\author{Matthew C. McFee}
\title{Advanced Computational Biology: Assignment 5}
\date{June 11, 2021}

\maketitle
\section{Part 1: Dimensionality Reduction}

\subsection{PCA}
\begin{figure}[!htb]
      \includegraphics[width = 0.5\linewidth]{dimensionality_reduction/pca.png}
      \hfill
      \includegraphics[width = 0.5\linewidth]{dimensionality_reduction/pca_top_500.png}
      \caption{(Left) PCA performed on all features (Right) PCA performed on the 500
        features with greatest variance.}
\end{figure}

\begin{figure}[!htb]
  \centering
  \includegraphics[width = 0.5\linewidth]{dimensionality_reduction/scree.png}
  \caption{Scree plot of the first 10 prinicipal components of the PCA performed on all features.}
\end{figure}

\textbf{Total variance explained in first 10 principle components:} ~55\%

\textbf{Number of components explaining $>$ 1\% of variance:} 8 


In this example dataset we are operating in a very high dimensional space so the
variance is "spread" across many dimensions. Although, it does appear that the first 10
principle components capture a large chunk of the variance it does not mean that there is
a small amount of variance in the other components. The only way to capture (or almost
all) would to have close to k (where k is the number of features) principle
components. It appears that only using the top 500 features with greatest variance did
not change the representation of the data in a 2D space much. This type of adjustment
makes sense as it is likely that a large portion of the features are not useful and
explain little variation in the data.

\subsection{NMF}
\begin{figure}[!htb]
  \centering
  \includegraphics[width = 0.5\linewidth]{dimensionality_reduction/nmf_var.png}
  \caption{The variance explained by each of the metagenes determined by NMF.}
\end{figure}

\subsubsection{Why do we have to randomly hide data from the matrix rather than hold out entire
  cells or genes?}
The idea of these dimensionality reduction techniques is to move from a very large feature space
down to a smaller feature space which recapitulates the important aspects of the variation in
the original data. Thus, in something like PCA we want to reduce our dimensions by
finding the directions of maximum variance and then projecting the high dimensional data
to this new feature space. In the case of PCA, projecting onto the directions of maximum
variance is equivalent to minimizing the reconstruction error. In
other words, if the dimensionality reduction procedure is good, then reconstructing the
data from the lower dimensional space back to the original space should have low
reconstruction error. We mask a subset of the data and then check its reconstruction
error of the masked values vs the the the data the NFM algorithm
saw. The idea is that there is some optimal k which will result in a W and H
that will best reconstruct the original matrix. This best value should result in the
originally missing "NA" values being close to their true values. If you hold out an
entire gene or cell by setting it to 0's then a row or column of W or H will be set to 0
which doesn't make sense. Furthermore, by checking the best r for the unseen data values
we are putting a limit on our value of r. Theoretically, you can simply keep increasing
r and the reconstruction error will always decrease. The best r value is about 10 as this is the point at which the
validation reconstruction error reaches a minimum. Increasing r does nothing to reduce
the validation set reconstruction error and thus we do not need any of those extra
features.

\begin{figure}[!htb]
  \centering
  \includegraphics[width = 0.33\linewidth]{dimensionality_reduction/nmf_var_seed_2.png}
  \includegraphics[width = 0.33\linewidth]{dimensionality_reduction/nmf_var_seed_99.png}
  \includegraphics[width = 0.33\linewidth]{dimensionality_reduction/nmf_var_seed_888.png}
  \caption{The variance explained by each of the metagenes determined by NMF with 3
        different seeds.}
\end{figure}


\begin{frame}{}
  \begin{figure}[!htb]
    \begin{minipage}{1\linewidth}
      \centering
      \includegraphics[width = 0.5\linewidth]{dimensionality_reduction/h_heatmap.png}
      \caption{Heatmap representation of the H matrix calculated by the NMF technique.}
    \end{minipage}
  \end{figure}
\end{frame}

\subsubsection{The variances for each random seed of NMF and the interpretation of the H
  matrix}
The algorithm being used to compute the nonnegative matrix factorization appears to
generally converge to the same matrices. This is because the matrix initialization used
was not randomized. The solver is coordinate descent which works similarly to gradient
descent but one parameter at a time. This means each time the same matrix is being
coverged to. Essentially, the H matrix we are getting is a Sample x Metagene matrix. It
has a clustering property such that the metagene most highly expressed by any given
sample corresponds to its assigned cluster. If H is specified to be orthogonal,
obtaining H is mathematically equivalent to the k-means clustering method. For example,
entries (rows) 28-55 in the data correspond to DC1 type cells and it can be seen on the heatmap
that all of these entries have high values in the same metagenes.

\textbf{Useful papers}

\href{https://www.pnas.org/content/101/12/4164}{https://www.pnas.org/content/101/12/4164}

\href{https://ranger.uta.edu/~chqding/papers/NMF-SDM2005.pdf}{https://ranger.uta.edu/~chqding/papers/NMF-SDM2005.pdf}

\subsection{tSNE}

\begin{figure}[!htb]
  \centering
  \includegraphics[width = 0.5\linewidth]{dimensionality_reduction/tsne.png}
  \caption{The tSNE dimensionality reduction technique performed on the dataset.}
\end{figure}

\begin{figure}[!htb]
  \centering
  \includegraphics[width = 0.33\linewidth]{dimensionality_reduction/tsne_perplexity_2.png}
  \includegraphics[width = 0.33\linewidth]{dimensionality_reduction/tsne_perplexity_4.png}
  \includegraphics[width = 0.33\linewidth]{dimensionality_reduction/tsne_perplexity_12.png}
  \caption{A comparison of different perplexity values.}
\end{figure}

\begin{figure}[!htb]
    \centering
    \includegraphics[width = 0.33\linewidth]{dimensionality_reduction/tsne_random_seed_6.png}
    \includegraphics[width = 0.33\linewidth]{dimensionality_reduction/tsne_random_seed_66.png}
    \includegraphics[width = 0.33\linewidth]{dimensionality_reduction/tsne_random_seed_666.png}
    \caption{A comparison of different random initializations.}
\end{figure}

\subsection{Comments on perplexity, random initialization and the best method}
Mathematically, the perplexity controls the variance of the Gaussians centered at each
high dimensional data point. These Gaussians are used to compute similarities between
points in the high dimensional space and can be though of determining which points are
the nearest neighbors of any given point. It effectively pulls the points together into
tighter cluster as seen in the figures.

The tSNE algorithm is completely dependent on a random initialization and thus seed
selection will impact the displayed results but shouldn't change which data points are
closly clustered as seen in the figures.

There are pros and cons to using each of these dimenstionality reduction techniques. For
example PCA is very easy to use and has a deterministic solution (you will reach the
same principal components and vector projections each time), it is computationally
simple but may require a large amount of memory. NMF is useful in that it also provides
a means of easily clustering similar data points but the algorithms used to find its
matrices are not guaranteed to find the global optimum and by definition the matrix to
be factorized must have only nonnegative elements which restricts it to use cases where
this makes sense (genetics, image rgb values etc.). tSNE is the most computationally
complex of the factorization methods. It's strength is preserving local structures
generating interpretable 2D visualizations, and works very well with large
datasets. Furthermore, by definition tSNE can never be reconstructed to the original
vector space.

In the case of our dataset tSNE performs the best because we have a very large number of
features (22430). It more tightly clusters the data in comparison to PCA/NMF and
exluding DC5 all of the DC clusters are very distinct from each other.

\section{Part 2: Classification}

\subsection{Multinomial Regression and L1/L2 Regularization}

\begin{figure}[!htb]
  \centering
  \includegraphics[width = 0.4\linewidth]{classification/multinomial_regression/multinomial_regression_None_regularization_loss.png}
  \includegraphics[width = 0.4\linewidth]{classification/multinomial_regression/multinomial_regression_None_regularization_accuracy.png}
  \includegraphics[width = 0.4\linewidth]{classification/multinomial_regression/multinomial_regression_L1_regularization_loss.png}
  \includegraphics[width = 0.4\linewidth]{classification/multinomial_regression/multinomial_regression_L1_regularization_accuracy.png}
  \includegraphics[width = 0.4\linewidth]{classification/multinomial_regression/multinomial_regression_L2_regularization_loss.png}
  \includegraphics[width = 0.4\linewidth]{classification/multinomial_regression/multinomial_regression_L2_regularization_accuracy.png}
  \caption{Training loss and accuracy curves for various regularization methods.  }
\end{figure}

By design the L1 and L2 regularization should increase the training loss and training
accuracy as these regularization methods are meant to prevent overfitting. It can be
seen that this model rapidly converges to model parameters that very accurately predict
both the train and test set labels. This dataset MNIST is a very simple problem so this
makes sense. L1 is a much more aggressive regularization scheme as it tries to push as
many model weights to 0 as possible (sparse weight matrix) in comparison to L2 regularization which simply
attempts to make the model weights smaller. The test loss is slightly lower as well
which also results in the test accuracy being slightly higher. This is likely due to the
similarity of the two sets of data and the test set being slightly smaller. I can
confirm that we get the standard test loss $>$ train loss and test accuracy $<$ train
accuracy if I use 5000 random data points from each of the sets for training and
testing (Figure 10). The L2 parameter I used was 0.5 and the L1 parameter was 0.1. The L1
regularization appears very strong and could be lowered. The L2 parameter seems to be
working well. The learning rate was 0.001 for all cases.

\begin{figure}[!htb]
  \centering
  \includegraphics[width = 0.4\linewidth]{classification/training_test/test_multinomial_small_data_loss.png}
  \includegraphics[width = 0.4\linewidth]{classification/training_test/test_multinomial_small_data_accuracy.png}
  \caption{Training curves for a small subset of the data (5000 points from the full training set and 5000 from the test set) shows that the model works and makes sense.}
\end{figure}

\subsection{Support Vector Machines}
The linear SVM performs quite well on the dataset since this is a relatively simple
problem. It scores roughly 97\% on the training set and 93\% on the test set. This
indicates that the classes can be separated with linear hyperplanes. The RBF kernel is
essentially an application of the kernel trick which involves working with
the data in some higher dimensional space such that it is easier to separate the data
with a linear hyperplane without ever actually transforming the data to said higher
dimensional space. This is because the dot product of the transformed data can be
expressed in terms of the lower dimensional data. The performance for the RBF SVM is
roughly 99\% for the training set and 97\% for the test set. Since our performance increased
slightly it means that it helped the data to become more linearly separable. It is
important to remember that we never actually move our points to the higher dimensional
space but we do work in the higher dimensional space. The radial basis function uses a
Gaussian to determine the similarity between two points which is they key to the kernel
trick. Also, it appears the linear SVM has difficulty converging.

\subsection{Multinomial Regression with a Hidden Layer and ReLU Activation}
\begin{figure}[!htb]
  \centering
  \includegraphics[width = 0.4\linewidth]{classification/multinomial_regression_relu/multinomial_regression_relu_None_regularization_loss.png}
  \includegraphics[width = 0.4\linewidth]{classification/multinomial_regression_relu/multinomial_regression_relu_None_regularization_accuracy.png}
  \includegraphics[width = 0.4\linewidth]{classification/multinomial_regression_relu/multinomial_regression_relu_L1_regularization_loss.png}
  \includegraphics[width = 0.4\linewidth]{classification/multinomial_regression_relu/multinomial_regression_relu_L1_regularization_accuracy.png}
  \includegraphics[width = 0.4\linewidth]{classification/multinomial_regression_relu/multinomial_regression_relu_L2_regularization_loss.png}
  \includegraphics[width = 0.4\linewidth]{classification/multinomial_regression_relu/multinomial_regression_relu_L2_regularization_accuracy.png}
  \caption{Training loss and accuracy curves for various regularization methods with
        ReLU activation.}
\end{figure}
  
\begin{figure}[!htb]
  \centering
  \includegraphics[width = 0.4\linewidth]{classification/multinomial_regression_relu/multinomial_regression_relu_0.001_learning_rate_loss.png}
  \includegraphics[width = 0.4\linewidth]{classification/multinomial_regression_relu/multinomial_regression_relu_0.001_learning_rate_accuracy.png}
  \includegraphics[width = 0.4\linewidth]{classification/multinomial_regression_relu/multinomial_regression_relu_0.01_learning_rate_loss.png}
  \includegraphics[width = 0.4\linewidth]{classification/multinomial_regression_relu/multinomial_regression_relu_0.01_learning_rate_accuracy.png}
      
  \includegraphics[width = 0.4\linewidth]{classification/multinomial_regression_relu/multinomial_regression_relu_0.1_learning_rate_loss.png}
  \includegraphics[width = 0.4\linewidth]{classification/multinomial_regression_relu/multinomial_regression_relu_0.1_learning_rate_accuracy.png}
  \caption{Training curves for various learning rates for the multinomial regression
      with a hidden layer and ReLU activation.}
\end{figure}

As discussed in lecture all combinations of linear functions result in another linear
function. Thus, to model more complex non-linear functions non-lienar activation
functions are added to neural networks. The linear combination of the previous layers
output is passed through this function. In this case the non-linear activation function
we are using is ReLU which essentially keeps the linear combination of the inputs if it
is positive and sets it to 0 if it is negative. Here no regularization appeared to work
the best. L1 regularization with a parameter of 0.1 was too harsh and caused poor
performance. L2 regularization with a parameter of 0.5 eventually caused the losses to
increase.

The learning rate controls how the weights of the model are updated via a gradient
descent method. Thus, a larger learning rate will result in large magnitude changes in
the model parameters. This may result in the weights bouncing around but never reaching
the optimum or it may cause the weight parameters to rapidly move away from the optimum
and the loss ``exploding''. A too small learning rate may result in too slow learning
which would require too many weight updates. Here a learning rate of 0.001 seemed to
reach the best solution but converged the slowest.

\subsection{Convolutional Neural Network}

\begin{figure}[!htb]
  \centering
  \includegraphics[width = 0.4\linewidth]{classification/conv_results/convnet_loss.png}
  \includegraphics[width = 0.4\linewidth]{classification/conv_results/convnet_accuracy.png}
  \caption{Training curves for a simple convolutional network trained on the MNIST data}
\end{figure}

Here I have used two convolutional layers without any extensive regularization such as
dropout on the fully connected layers. It is very common to apply pooling after each
convolutional layer as well. I used a small number of channels for my convolutional
layers with the 5 generating 5 channels from the single input channel and the second
creating 10 channels from 5. A kernel of 5 was used (common sizes are 2x2, 3x3,
5x5). This could likely be decreased in size to reduce the number of model parameters
with performance being maintained. I kept the stride at 1 as well. Finally, the
convolutional layers output is flattened and passed through two linear layers to reach
an output dimension of 10. Really all of these are model parameters that would be
carefully tuned during training of a real model. Convolutional neural networks are so
effective for computer vision tasks as various kernels that generate each feature map
will learn how to detect different features in the images (for example a kernel may
generate a feature map that highlights edges or lighting gradients in the image) using
local information as it moves across the image. The features detected by the kernels
becoming increasingly abstract as depth increases. We do not need many convolutional
layers for this task as the images are very simple. It can be seen that this simple
convolutional network easily outperforms the multinomial regression that was first conducted.

\textbf{Useful links:}

\href{https://nextjournal.com/gkoehler/pytorch-mnist}{https://nextjournal.com/gkoehler/pytorch-mnist}

\href{https://pytorch.org/tutorials/recipes/recipes/defining\_a\_neural\_network.html}{https://pytorch.org/tutorials/recipes/recipes/defining\_a\_neural\_network.html}

\href{https://www.cs.toronto.edu/~lczhang/321/tut/tut06.html}{https://www.cs.toronto.edu/~lczhang/321/tut/tut06.html}

\end{document}