# Advanced Computational Biology 

# Assignment 5

# Matthew C. McFee

Requirements: 
Python (3.8.5), numpy (1.19.4), pandas (1.1.5), sklearn, scikit-learn(0.24.0),
matplotlib (3.3.3), seaborn (0.11.1), torch (1.7.0)

Instructions:

All of the code has been run and the results and commentary are in assignment_2.pdf

To rerun the code

cd into the directories dimensionality_reduction and classification and run

python assignment_2_dimensionality_reduction.py
python assignment_2_classification.py

A jupyter notebook is also available for each part.
