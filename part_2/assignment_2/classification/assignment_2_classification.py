"""
Advanced Computationl Biology II
Assignment #2
Part 2: Classification

Matthew McFee

Notes: All model implementations are in PyTorch
"""

import numpy as np
import pandas as pd
import torch.nn as nn
import torch.optim as optim
import torch
import torch.nn.functional as F
import matplotlib.pyplot as plt


class MultinomialRegression(nn.Module):
    """Multinomial Regression Model in PyTorch"""

    def __init__(self):
        super().__init__()
        self.linear = nn.Linear(784, 10)
        self.softmax = nn.Softmax(dim=1)

    def forward(self, x):
        out = self.linear(x)
        # Softmax is already integrated into PyTorch's loss function
        # out = self.softmax(out)

        return out


def get_accuracy(model, criterion, batch_size, data):
    """Function to get model accuracy after epoch"""
    total_correct = 0
    softmax = nn.Softmax(dim=1)
    with torch.no_grad():
        for batch in data:
            x = torch.unsqueeze(batch[:, 1:], dim=1).double()
            out = model(x).squeeze()
            labels = batch[:, 0].long()

            loss = criterion(out, labels)
            out = softmax(out)
            classification = out.argmax(dim=1)

            total_correct += (classification == labels).float().sum()

    return total_correct / (len(data) * batch_size)


def train_model(
    train, test, model, epochs=40, batch_size=100, learning_rate=0.01, reg=None
):
    """Function to train the model"""
    # Handle regularization
    if reg is None:
        optimizer = optim.SGD(model.parameters(), lr=learning_rate)
    elif reg == "l1":
        optimizer = optim.SGD(model.parameters(), lr=learning_rate)
        l1_lambda = 0.05
    elif reg == "l2":
        optimizer = optim.SGD(model.parameters(), lr=learning_rate, weight_decay=0.5)
    else:
        raise ValueError("Regularization setting is not valid!")

    softmax = nn.Softmax(dim=1)

    criterion = nn.CrossEntropyLoss()

    loss_iterations = []
    train_loss = []
    test_loss = []
    train_accuracy = []
    test_accuracy = []

    train_size = train.shape[0]
    test_size = test.shape[0]

    num_train_batches = int(train_size / batch_size)
    num_test_batches = int(test_size / batch_size)

    for i in range(epochs):
        model.train()

        # Shuffle the data each epoch
        random_indices = torch.randperm(train_size)
        train = train[random_indices]
        random_indices_test = torch.randperm(test_size)
        test = test[random_indices_test]

        # Batch the data
        train_chunk = torch.chunk(train, num_train_batches, dim=0)
        test_chunk = torch.chunk(test, num_test_batches, dim=0)

        # Train the model on the data
        if i != 0:
            for batch in train_chunk:
                x = torch.unsqueeze(batch[:, 1:], dim=1).double()
                out = model(x).squeeze()
                labels = batch[:, 0].long()

                loss = criterion(out, labels)

                if reg == "l1":
                    # Reference: https://pytorch.org/assets/deep-learning/Deep-Learning-with-PyTorch.pdf
                    l1_norm = sum(p.abs().sum() for p in model.parameters())
                    loss = loss + l1_lambda * l1_norm

                loss.backward()

                optimizer.step()
                optimizer.zero_grad()

                loss_iterations.append(loss.item())

        # Set model to eval mode
        model.eval()

        # Set lists up for current epoch
        train_current = 0
        test_current = 0
        train_correct = 0
        test_correct = 0

        # Check model performance on training and test set
        with torch.no_grad():
            for batch in train_chunk:
                x = torch.unsqueeze(batch[:, 1:], dim=1).double()
                out = model(x).squeeze()
                labels = batch[:, 0].long()

                loss = criterion(out, labels)

                train_current += loss.item() * batch_size

                out = softmax(out)
                classification = out.argmax(dim=1)

                train_correct += (classification == labels).float().sum()

            for batch in test_chunk:
                x = torch.unsqueeze(batch[:, 1:], dim=1).double()
                out = model(x).squeeze()
                labels = batch[:, 0].long()

                loss = criterion(out, labels)

                test_current += loss.item() * batch_size

                out = softmax(out)
                classification = out.argmax(dim=1)

                test_correct += (classification == labels).float().sum()

        # Store model performance for current epoch
        train_loss.append(train_current / train_size)
        test_loss.append(test_current / test_size)
        train_accuracy.append(train_correct / train_size)
        test_accuracy.append(test_correct / test_size)
        print(
            "Epoch: ",
            i,
            "train accuracy: ",
            train_accuracy[-1].item(),
            "test accuracy: ",
            test_accuracy[-1].item(),
        )

    return loss_iterations, train_loss, test_loss, train_accuracy, test_accuracy


# In[5]:


# One hot encode the classes not necessary for PyTorch
train = pd.read_csv("mnist_train.csv", header=None)

test = pd.read_csv("mnist_test.csv", header=None)

train = train.values
test = test.values

# Convert data to tensors
train = torch.from_numpy(train)
test = torch.from_numpy(test)

regularization_methods = [None, "l1", "l2"]

for method in regularization_methods:
    model_mr = MultinomialRegression().double()

    (
        loss_iterations_mr,
        train_loss_mr,
        test_loss_mr,
        train_accuracy_mr,
        test_accuracy_mr,
    ) = train_model(
        train, test, model_mr, epochs=40, batch_size=100, learning_rate=0.01, reg=method
    )

    # plt.plot(range(1, len(loss_iterations_mr) + 1), loss_iterations_mr)
    plt.plot(range(1, len(train_loss_mr) + 1), train_loss_mr)
    plt.plot(range(1, len(test_loss_mr) + 1), test_loss_mr)
    plt.title("Training Loss: " + str(method) + " regularization")
    plt.xlabel("Epochs")
    plt.ylabel("Categorical Cross Entropy")
    plt.legend(["train", "test"])

    file_name = "multinomial_regression_" + str(method) + "_regularization_loss.png"
    plt.savefig(file_name)
    plt.clf()

    plt.plot(range(1, len(train_accuracy_mr) + 1), train_accuracy_mr)
    plt.plot(range(1, len(test_accuracy_mr) + 1), test_accuracy_mr)
    plt.title("Training Accuracy: " + str(method) + " regularization")
    plt.xlabel("Epochs")
    plt.ylabel("Accuracy (%)")
    plt.legend(["train", "test"])

    file_name = "multinomial_regression_" + str(method) + "_regularization_accuracy.png"
    plt.savefig(file_name)
    plt.clf()


# By design the L1 and L2 regularization should increase the training loss and training accuracy as these regularization methods are meant to prevent overfitting. It can be seen that this model rapidly converges to model parameters that very accurately predict both the train and test set labels. This dataset MNIST is a very simple problem so this makes sense. L1 is a much more aggressive regularization scheme as it tries to push as many model weights to 0 as possible in comparison to L2 regularization which simply attempts to make the model weights smaller.

# Part 2: Support Vector Machines
from sklearn import svm

train = pd.read_csv("mnist_train.csv", header=None)
test = pd.read_csv("mnist_test.csv", header=None)

X_train = train.values[:, 1:]
y_train = train.values[:, 0]

X_test = test.values[:, 1:]
y_test = test.values[:, 0]

linear_svm = svm.SVC(kernel="linear", max_iter=50000)
linear_svm.fit(X_train, y_train)

lsvm_test = linear_svm.score(X_test, y_test)

rbf_svm = svm.SVC(kernel="rbf", max_iter=50000)
rbf_svm.fit(X_train, y_train)

rsvm_test = rbf_svm.score(X_test, y_test)

lsvm_train = linear_svm.score(X_train, y_train)
rsvm_train = rbf_svm.score(X_train, y_train)

with open("svm_results.txt", "w") as svm_file:
    svm_file.write("Linear SVM Accuracy" + "\n")
    svm_file.write("Train: " + str(lsvm_train) + "\n")
    svm_file.write("Test: " + str(lsvm_test) + "\n")
    svm_file.write("RBF SVM Accuracy" + "\n")
    svm_file.write("Train: " + str(rsvm_train) + "\n")
    svm_file.write("Test: " + str(rsvm_test) + "\n")


# The linear SVM performs quite well on the test set since this is a relatively simple problem. This indicates that the classes can be separated with linear hyperplanes. The RBF kernel is essentially an application of the kernel trick which involves working with the data in some higher dimensional space such that it is easier to separate the data with a linear hyperplane without ever actually transforming the data to said higher dimensional space. This is because the dot product of the transformed data can be expressed in terms of the lower dimensional data. Since our performance increased slightly it means that it helped the data to become more linearly separable. It is important to remember that we never actually move our points to the higher dimensional space but we do work in the higher dimensional space. The radial basis function uses a Gaussian to determine the similarity between two points which is they key to the kernel trick. Also it appears the linear SVM has difficulty converging.

# Data again
train = pd.read_csv("mnist_train.csv", header=None)

test = pd.read_csv("mnist_test.csv", header=None)

train = train.values
test = test.values

# Convert data to tensors
train = torch.from_numpy(train)
test = torch.from_numpy(test)

# Part C: Adding a hidden layer to the network
class MultinomialRegression2(nn.Module):
    """Multinomial Regression with ReLU activation"""

    def __init__(self):
        super().__init__()
        self.linear_1 = nn.Linear(784, 800)
        self.linear_2 = nn.Linear(800, 10)

    def forward(self, x):
        out = self.linear_1(x)
        out = F.relu(out)
        out = self.linear_2(out)
        out = F.relu(out)

        return out


regularization_methods = [None, "l1", "l2"]

for method in regularization_methods:
    model_mr = MultinomialRegression2().double()

    (
        loss_iterations_mr2,
        train_loss_mr2,
        test_loss_mr2,
        train_accuracy_mr2,
        test_accuracy_mr2,
    ) = train_model(
        train,
        test,
        model_mr2,
        epochs=40,
        batch_size=100,
        learning_rate=0.001,
        reg=method,
    )

    plt.plot(range(1, len(train_loss_mr2) + 1), train_loss_mr2)
    plt.plot(range(1, len(test_loss_mr2) + 1), test_loss_mr2)
    plt.title("Training Loss: " + str(method) + " regularization")
    plt.xlabel("Epochs")
    plt.ylabel("Categorical Cross Entropy")
    plt.legend(["train", "test"])

    file_name = (
        "multinomial_regression_relu_" + str(method) + "_regularization_loss.png"
    )
    plt.savefig(file_name)
    plt.clf()

    plt.plot(range(1, len(train_accuracy_mr2) + 1), train_accuracy_mr2)
    plt.plot(range(1, len(test_accuracy_mr2) + 1), test_accuracy_mr2)
    plt.title("Training Accuracy: " + str(method) + " regularization")
    plt.xlabel("Epochs")
    plt.ylabel("Accuracy (%)")
    plt.legend(["train", "test"])

    file_name = (
        "multinomial_regression_relu_" + str(method) + "_regularization_accuracy.png"
    )
    plt.savefig(file_name)
    plt.clf()


possible_learning_rates = [0.001, 0.01, 0.1]

for possible_lr in possible_learning_rates:
    model_mr3 = MultinomialRegression2().double()

    (
        loss_iterations_mr3,
        train_loss_mr3,
        test_loss_mr3,
        train_accuracy_mr3,
        test_accuracy_mr3,
    ) = train_model(
        train,
        test,
        model_mr3,
        epochs=40,
        batch_size=100,
        learning_rate=possible_lr,
        reg=None,
    )

    # plt.plot(range(1, len(loss_iterations_mr) + 1), loss_iterations_mr)
    plt.plot(range(1, len(train_loss_mr3) + 1), train_loss_mr3)
    plt.plot(range(1, len(test_loss_mr3) + 1), test_loss_mr3)
    plt.title("Training Loss: " + str(possible_lr) + " learning rate")
    plt.xlabel("Epochs")
    plt.ylabel("Categorical Cross Entropy")
    plt.legend(["train", "test"])
    # plt.show()
    file_name = (
        "multinomial_regression_relu_" + str(possible_lr) + "_learning_rate_loss.png"
    )
    plt.savefig(file_name)
    plt.clf()

    plt.plot(range(1, len(train_accuracy_mr3) + 1), train_accuracy_mr3)
    plt.plot(range(1, len(test_accuracy_mr3) + 1), test_accuracy_mr3)
    plt.title("Training Accuracy: " + str(possible_lr) + " learning rate")
    plt.xlabel("Epochs")
    plt.ylabel("Accuracy (%)")
    plt.legend(["train", "test"])
    # plt.show()
    file_name = (
        "multinomial_regression_relu_"
        + str(possible_lr)
        + "_learning_rate_accuracy.png"
    )
    plt.savefig(file_name)
    plt.clf()


# Here I have used two convolutional layers without an extensive regularization such as dropout on the fully connected layers. It is very common to apply pooling after each convolutional layer. I used a small number of channels for my convolutional layers with the 5 generating 5 channels from the single input channel and the second creating 10 channels from 5. A kernel of 5 was used (common sizes are 2x2, 3x3, 5x5). This could likely be decreased in size to reduce the number of model parameters with performance being maintained. I kept the stride at 1 as well. Finally, the convolutional layers output is flattened and passed through two linear layers to reach an output dimension of 10. Really all of these are model parameters that would be carefully tuned during training of a real model.
#
# Useful references:
# https://nextjournal.com/gkoehler/pytorch-mnist
#
# https://pytorch.org/tutorials/recipes/recipes/defining_a_neural_network.html


class ConvNet(nn.Module):
    """Simple ConvNet with two convolutional layers and two linear layers"""

    def __init__(self):
        super().__init__()
        self.conv_1 = nn.Conv2d(1, 5, kernel_size=5)
        self.conv_2 = nn.Conv2d(5, 10, kernel_size=5)
        self.linear_1 = nn.Linear(160, 80)
        self.linear_2 = nn.Linear(80, 10)

    def forward(self, x):
        out = F.relu(F.max_pool2d(self.conv_1(x), 2))
        out = F.relu(F.max_pool2d(self.conv_2(out), 2))
        out = out.view(-1, 160)
        out = F.relu(self.linear_1(out))
        out = self.linear_2(out)

        return out


# Prepare the data, need to unflatten the images
# One hot encode the classes not necessary for PyTorch
train = pd.read_csv("mnist_train.csv", header=None)
test = pd.read_csv("mnist_test.csv", header=None)

train_data = train.values[:, 1:]
test_data = test.values[:, 1:]

train_labels = train.values[:, 0]
test_labels = test.values[:, 0]

train_data = train_data.reshape(-1, 28, 28)
test_data = test_data.reshape(-1, 28, 28)

# Convert data to tensors
train_data = torch.from_numpy(train_data)
test_data = torch.from_numpy(test_data)
train_labels = torch.from_numpy(train_labels)
test_labels = torch.from_numpy(test_labels)

train = [train_data, train_labels]
test = [test_data, test_labels]


def train_model_conv(
    train, test, model, epochs=40, batch_size=100, learning_rate=0.01, reg=None
):
    """Function to train the model modified for 28x28 images"""
    # Handle regularization
    if reg is None:
        optimizer = optim.SGD(model.parameters(), lr=learning_rate)
    elif reg == "l1":
        optimizer = optim.SGD(model.parameters(), lr=learning_rate)
        l1_lambda = 0.1
    elif reg == "l2":
        optimizer = optim.SGD(model.parameters(), lr=learning_rate, weight_decay=0.1)
    else:
        raise ValueError("Regularization setting is not valid!")

    softmax = nn.Softmax(dim=1)

    criterion = nn.CrossEntropyLoss()

    loss_iterations = []
    train_loss = []
    test_loss = []
    train_accuracy = []
    test_accuracy = []

    train_data = train[0]
    train_labels = train[1]

    test_data = test[0]
    test_labels = test[1]

    train_size = train_data.shape[0]
    test_size = test_data.shape[0]

    num_train_batches = int(train_size / batch_size)
    num_test_batches = int(test_size / batch_size)

    for i in range(epochs):
        model.train()

        # Shuffle the data each epoch
        random_indices = torch.randperm(train_size)
        train_data = train_data[random_indices]
        random_indices_test = torch.randperm(test_size)
        test_data = test_data[random_indices_test]

        train_labels = train_labels[random_indices]
        test_labels = test_labels[random_indices_test]

        # Batch the data
        train_data_chunk = torch.chunk(train_data, num_train_batches, dim=0)
        test_data_chunk = torch.chunk(test_data, num_test_batches, dim=0)
        train_labels_chunk = torch.chunk(train_labels, num_train_batches, dim=0)
        test_labels_chunk = torch.chunk(test_labels, num_test_batches, dim=0)

        # Lazy fix but the iterable is consumed so make two copies in memory
        train_chunk = zip(train_data_chunk, train_labels_chunk)
        train_chunk_2 = zip(train_data_chunk, train_labels_chunk)
        test_chunk = zip(test_data_chunk, test_labels_chunk)

        # Train the model on the data
        if i != 0:
            for batch, labels in train_chunk:
                x = torch.unsqueeze(batch, dim=1).double()
                out = model(x).squeeze()
                labels = labels.long()

                loss = criterion(out, labels)

                if reg == "l1":
                    # Reference: https://pytorch.org/assets/deep-learning/Deep-Learning-with-PyTorch.pdf
                    l1_norm = sum(p.abs().sum() for p in model.parameters())
                    loss = loss + l1_lambda * l1_norm

                loss.backward()

                optimizer.step()
                optimizer.zero_grad()

                loss_iterations.append(loss.item())

        # Set model to eval mode
        model.eval()

        # Set lists up for current epoch
        train_current = 0
        test_current = 0
        train_correct = 0
        test_correct = 0

        # Check model performance on training and test set
        with torch.no_grad():
            for batch, labels in train_chunk_2:
                x = torch.unsqueeze(batch, dim=1).double()
                out = model(x).squeeze()
                labels = labels.long()

                loss = criterion(out, labels)

                train_current += loss.item() * batch_size

                out = softmax(out)
                classification = out.argmax(dim=1)

                train_correct += (classification == labels).float().sum()

            for batch, labels in test_chunk:
                x = torch.unsqueeze(batch, dim=1).double()
                out = model(x).squeeze()
                labels = labels.long()

                loss = criterion(out, labels)

                test_current += loss.item() * batch_size

                out = softmax(out)
                classification = out.argmax(dim=1)

                test_correct += (classification == labels).float().sum()

        # Store model performance for current epoch
        train_loss.append(train_current / train_size)
        test_loss.append(test_current / test_size)
        train_accuracy.append(train_correct / train_size)
        test_accuracy.append(test_correct / test_size)
        print(
            "Epoch: ",
            i,
            "train accuracy: ",
            train_accuracy[-1].item(),
            "test accuracy: ",
            test_accuracy[-1].item(),
        )

    return loss_iterations, train_loss, test_loss, train_accuracy, test_accuracy


# Train the convolutional network
model_conv = ConvNet().double()

(
    loss_iterations_conv,
    train_loss_conv,
    test_loss_conv,
    train_accuracy_conv,
    test_accuracy_conv,
) = train_model_conv(
    train, test, model_conv, epochs=40, batch_size=100, learning_rate=0.01, reg=None
)

plt.plot(range(1, len(train_loss_conv) + 1), train_loss_conv)
plt.plot(range(1, len(test_loss_conv) + 1), test_loss_conv)
plt.title("Training Loss Convolutional Network")
plt.xlabel("Epochs")
plt.ylabel("Categorical Cross Entropy")
plt.legend(["train", "test"])

file_name = "convnet_loss.png"
plt.savefig(file_name)
plt.clf()

plt.plot(range(1, len(train_accuracy_conv) + 1), train_accuracy_conv)
plt.plot(range(1, len(test_accuracy_conv) + 1), test_accuracy_conv)
plt.title("Training Accuracy Convolutional Network")
plt.xlabel("Epochs")
plt.ylabel("Accuracy (%)")
plt.legend(["train", "test"])

file_name = "convnet_accuracy.png"
plt.savefig(file_name)
plt.clf()


# Check Training on smaller subset since test accuracy slightly higher than train
train = pd.read_csv("mnist_train.csv", header=None)

test = pd.read_csv("mnist_test.csv", header=None)

train = train.values
test = test.values

# Convert data to tensors
train = torch.from_numpy(train)
test = torch.from_numpy(test)

model_test = MultinomialRegression().double()
(
    loss_iterations_test,
    train_loss_test,
    test_loss_test,
    train_accuracy_test,
    test_accuracy_test,
) = train_model(
    train[:5000],
    test[:5000],
    model_test,
    epochs=40,
    batch_size=100,
    learning_rate=0.01,
    reg=None,
)

plt.plot(range(1, len(train_loss_test) + 1), train_loss_test)
plt.plot(range(1, len(test_loss_test) + 1), test_loss_test)
plt.title("Training Loss: Smaller dataset")
plt.xlabel("Epochs")
plt.ylabel("Categorical Cross Entropy")
plt.legend(["train", "test"])

file_name = "test_multinomial_small_data_loss.png"
plt.savefig(file_name)
plt.clf()

plt.plot(range(1, len(train_accuracy_test) + 1), train_accuracy_test)
plt.plot(range(1, len(test_accuracy_test) + 1), test_accuracy_test)
plt.title("Training Accuracy: Smaller dataset")
plt.xlabel("Epochs")
plt.ylabel("Accuracy (%)")
plt.legend(["train", "test"])

file_name = "test_multinomial_small_data_accuracy.png"
plt.savefig(file_name)
plt.clf()
