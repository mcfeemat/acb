# Advanced Computational Biology 

# Assignment 4

# Matthew C. McFee

Requirements: 
Python (3.8.5), numpy (1.19.4)

Instructions:

All of the code has been run and the results and commentary are in assignment_1.pdf

To rerun the code

cd into the directory and run the command

python assignment_1.py

A jupyter notebook is also available.
