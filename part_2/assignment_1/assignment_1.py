"""
Matthew McFee

ACB Part 2
Assignment 1

Notes: Remember to report all offsets as +1 as Python uses 0-indexing
"""

import numpy as np
import time
import matplotlib.pyplot as plt


# Helper functions
def convert_seq(seq):
    """Convert nucleotide sequence to integer sequencer"""
    mapping = {"A": 1, "C": 2, "G": 3, "U": 4}

    converted_seq = [mapping[char] for char in seq]

    return converted_seq


def find_k_mers(seq, k):
    """Find all k-mers in a sequence"""
    kmers = []
    for i in range(len(seq) - k + 1):
        start = i
        stop = i + 4
        current_kmer = []

        for j in range(start, stop):
            current_kmer.append(seq[j])

        kmers.append(current_kmer)

    return kmers


def convert_text_to_list(inputs_path):
    """Convert text file of sequences to a Python list"""
    inputs = []
    with open(inputs_path, "r") as file:
        for line in file:
            inputs.append(line.rstrip())

    return inputs


def max_offset(input_seq, k):
    """Find maximum motif offset"""
    o_max = len(input_seq) - k

    return o_max


def write_file(name, runtime, e_m, offset, pfm):
    """Write algorithm results to text file"""
    file_name = name + ".txt"

    with open(file_name, "w") as file:
        file.write("Algorithm: " + name + "\n")
        file.write("Runtime (s): " + str(runtime) + "\n")
        file.write("logE(M,o): " + str(e_m) + "\n")
        file.write("Offset: " + str(offset) + "\n")
        file.write("PFM: " + "\n" + str(pfm) + "\n")

    return None


def plot_result(values, title, file_name):
    """Plot results as specified in assignment"""
    plt.plot(range(1, len(values) + 1), values)
    plt.xlabel("Iterations")
    plt.ylabel("log(E(m, o))")
    plt.title(title)

    # Save the plot
    save_as = file_name + ".png"
    plt.savefig(save_as)

    # Close and clear
    plt.clf()
    plt.close()

    return None


# Part 1 HardOOPs
def m_step(inputs, k, o):
    """Calculate the current M matrix"""
    a = np.repeat(np.array(0.25), k)
    # Initialize M
    M = np.zeros((4, k))
    for j, value in enumerate(a):
        M[:, j] = M[:, j] + value

    all_kmers = []

    for i, inp in enumerate(inputs):
        current_o = o[i]
        all_kmers.append(inp[current_o : current_o + k])

    # Go through each kmer and update M matrix accordingly
    for kmer in all_kmers:
        for j, i in enumerate(kmer):
            M[i - 1, j] = M[i - 1, j] + 1

    # Normalize
    column_sums = M.sum(axis=0)

    normalized_M = np.divide(M, column_sums)

    return normalized_M


def e_step(inputs, M, k):
    """Find offsets that maximize Pi/Qi for each input"""
    offsets = []
    probs = []

    for inp in inputs:
        o_min = 0
        o_max = max_offset(inp, k)
        o_best = 0
        p_q_best = 0
        current_p_qs = []

        for o in range(o_min, o_max + 1):
            current_kmer = inp[o : o + k]
            current_p = 1
            current_q = 1

            # Calculate current P
            for j, i in enumerate(current_kmer):
                current_p = current_p * M[i - 1, j]

            # Calculate current Q
            current_q = 0.25 ** k

            p_q = current_p / current_q

            # if p_q >= p_q_best:
            #    p_q_best = p_q
            #    o_best = o

            current_p_qs.append(p_q)

        p_q_best = max(current_p_qs)
        o_best = current_p_qs.index(p_q_best)

        offsets.append(o_best)
        probs.append(p_q_best)

    # Calculate E(m, o)
    probs = np.array(probs)
    e_m = np.log10(probs).sum()

    return e_m, offsets


def hardoops(inputs, k):
    """HardOOPs algorithm"""
    # Convert the inputs to a list of sequences
    # inputs = convert_text_to_list(inputs_path)
    converted_inputs = []

    for inp in inputs:
        curr_converted_inp = convert_seq(inp)
        converted_inputs.append(curr_converted_inp)

    # Hardcoded for now
    max_iters = 100
    epsilon = 0.001
    o_init = np.repeat(np.array(0), len(inputs))
    o_current = np.copy(o_init)
    e_m_old = 0
    e_m_current = 0
    e_m_log = []

    t_start = time.time()

    for i in range(max_iters):
        M_current = m_step(converted_inputs, k, o_current)

        e_m_old = e_m_current

        e_m_current, o_current = e_step(converted_inputs, M_current, k)

        e_m_log.append(e_m_current)

        if abs(e_m_current - e_m_old) <= epsilon:
            break

    t_stop = time.time()

    t_complete = t_stop - t_start

    # Add 1 to final returned offsets since Python uses 0-indexing
    o_final = [value + 1 for value in o_current]

    return e_m_current, o_final, M_current, t_complete, e_m_log


# Help function tests
test_seq = "UCUA"

test_out = convert_seq(test_seq)
print(test_out)

kmers_seq = "GCGACCAAGAAACGUGU"
kmers_seq = convert_seq(kmers_seq)

kmers_out = find_k_mers(kmers_seq, 4)
# print(kmers_out)
# print(len(kmers_out))

inputs_list = convert_text_to_list(
    "/home/matt/Dev/acb/part_2/assignment_1/sample_input.txt"
)

# print(inputs_list)

e_m, o, M, t, hardoops_log = hardoops(inputs_list, 6)


# Part 2 HardZOOPs
def m_step_hzoops(inputs, k, o):
    """Calculate the current M matrixf or HardZOOPs algorithm"""
    a = np.repeat(np.array(0.25), k)
    # Initialize M
    M = np.zeros((4, k))
    for j, value in enumerate(a):
        M[:, j] = M[:, j] + value

    all_kmers = []

    # converted_inputs = []

    # for inp in inputs:
    #    curr_converted_inp = convert_seq(inp)
    #    converted_inputs.append(curr_converted_inp)

    for i, inp in enumerate(inputs):
        current_o = o[i]

        # Ignore sequences that do not have a probability ratio > 1
        if current_o != -1:
            all_kmers.append(inp[current_o : current_o + k])
        else:
            continue

    # Go through each kmer and update M matrix accordingly
    for kmer in all_kmers:
        for j, i in enumerate(kmer):
            M[i - 1, j] = M[i - 1, j] + 1

    # Normalize
    column_sums = M.sum(axis=0)

    normalized_M = np.divide(M, column_sums)

    return normalized_M


def e_step_hzoops(inputs, M, k):
    """Find offsets that maximize Pi/Qi for each input ignoring certain values"""
    offsets = []
    probs = []

    for inp in inputs:
        o_min = 0
        o_max = max_offset(inp, k)
        o_best = 0
        p_q_best = 0
        current_p_qs = []

        for o in range(o_min, o_max + 1):
            current_kmer = inp[o : o + k]
            current_p = 1
            current_q = 1

            # Calculate current P
            for j, i in enumerate(current_kmer):
                current_p = current_p * M[i - 1, j]

            # Calculate current Q
            current_q = 0.25 ** k

            p_q = current_p / current_q

            # if p_q >= p_q_best:
            #    p_q_best = p_q
            #    o_best = o

            current_p_qs.append(p_q)

        p_q_best = max(current_p_qs)
        o_best = current_p_qs.index(p_q_best)

        # If the best
        if p_q_best <= 1:
            offsets.append(-1)
            probs.append(1)

        else:
            offsets.append(o_best)
            probs.append(p_q_best)

    # Calculate E(m, o)
    probs = np.array(probs)
    e_m = np.log10(probs).sum()

    return e_m, offsets


def hzoops(inputs, k):
    """HardZOOPs algorithm"""
    # Convert the inputs to a list of sequences
    # inputs = convert_text_to_list(inputs_path)
    converted_inputs = []
    e_m_log = []

    for inp in inputs:
        curr_converted_inp = convert_seq(inp)
        converted_inputs.append(curr_converted_inp)

    # Hardcoded for now
    max_iters = 100
    epsilon = 0.001
    o_init = np.repeat(np.array(0), len(inputs))
    o_current = np.copy(o_init)
    e_m_old = 0
    e_m_current = 0

    t_start = time.time()

    for i in range(max_iters):
        M_current = m_step_hzoops(converted_inputs, k, o_current)

        e_m_old = e_m_current

        e_m_current, o_current = e_step_hzoops(converted_inputs, M_current, k)

        e_m_log.append(e_m_current)

        if abs(e_m_current - e_m_old) <= epsilon:
            break

    t_stop = time.time()

    t_complete = t_stop - t_start

    # Add 1 to final returned offsets since Python uses 0-indexing
    o_final = [value + 1 for value in o_current]

    return e_m_current, o_final, M_current, t_complete, e_m_log


# Test HardZOOPs
e_m_hz, o_hz, M_hz, t_hz, hzoops_log = hzoops(inputs_list, 6)

# print(e_m_hz, o_hz, M_hz, t_hz)


# Part 3 GibbsZOOPs
def m_step_gzoops(inputs, k, o):
    """Calculate the current M matrix for GibbsZOOPs algorithm"""
    a = np.repeat(np.array(0.25), k)
    # Initialize M
    M = np.zeros((4, k))
    for j, value in enumerate(a):
        M[:, j] = M[:, j] + value

    all_kmers = []

    # converted_inputs = []

    # for inp in inputs:
    #    curr_converted_inp = convert_seq(inp)
    #    converted_inputs.append(curr_converted_inp)

    for i, inp in enumerate(inputs):
        current_o = o[i]

        if current_o != -1:
            all_kmers.append(inp[current_o : current_o + k])
        else:
            continue

    # Go through each kmer and update M matrix accordingly
    for kmer in all_kmers:
        for j, i in enumerate(kmer):
            M[i - 1, j] = M[i - 1, j] + 1

    # Normalize
    column_sums = M.sum(axis=0)

    normalized_M = np.divide(M, column_sums)

    return normalized_M


def gibbs_sample(values, probs):
    """Sample from custom discrete distribution"""
    o_sampled = np.random.choice(values, p=probs)

    return o_sampled


def e_step_gzoops(inputs, M, k):
    """E step for GibbsZOOPs algorithm"""
    offsets = []
    # r_o = 1
    probs = []

    for inp in inputs:
        o_min = 0
        o_max = max_offset(inp, k)
        # o_best = 0
        # p_q_best = 0
        current_p_qs = [1]

        possible_o = [-1]
        possible_o.extend(range(o_min, o_max + 1))

        for o in range(o_min, o_max + 1):
            current_kmer = inp[o : o + k]
            current_p = 1
            current_q = 1

            # Calculate current P
            for j, i in enumerate(current_kmer):
                current_p = current_p * M[i - 1, j]

            # Calculate current Q
            current_q = 0.25 ** k

            p_q = current_p / current_q

            current_p_qs.append(p_q)

        # Gibbs sampling
        norm = sum(current_p_qs)

        normalized_p_qs = [prob / norm for prob in current_p_qs]

        o_sampled = gibbs_sample(possible_o, normalized_p_qs)

        probs.append(current_p_qs[o_sampled + 1])
        offsets.append(o_sampled)

    # Calculate E(m, o)
    probs = np.array(probs)
    e_m = np.log10(probs).sum()

    return e_m, offsets


def gzoops(inputs, k):
    """GibbsZOOPs algorithm"""
    # Convert the inputs to a list of sequences
    # inputs = convert_text_to_list(inputs_path)
    converted_inputs = []
    e_m_log = []

    for inp in inputs:
        curr_converted_inp = convert_seq(inp)
        converted_inputs.append(curr_converted_inp)

    # Hardcoded for now
    t_max = 1000
    o_init = np.repeat(np.array(0), len(inputs))
    o_current = np.copy(o_init)
    e_m_old = 0
    e_m_current = 0
    e_m_best = 0

    t_start = time.time()

    for i in range(t_max):
        M_current = m_step_gzoops(converted_inputs, k, o_current)

        e_m_old = e_m_current

        e_m_current, o_current = e_step_gzoops(converted_inputs, M_current, k)

        e_m_log.append(e_m_current)

        if e_m_current > e_m_best:
            e_m_best = e_m_current
            o_best = o_current
            M_best = M_current

    t_stop = time.time()

    t_complete = t_stop - t_start

    # Add 1 to final returned offsets since Python uses 0-indexing
    o_final = [value + 1 for value in o_best]

    return e_m_best, o_final, M_best, t_complete, e_m_log


e_m_gz, o_gz, M_gz, t_gz, gzoops_log = gzoops(inputs_list, 6)
print(e_m_gz, o_gz, M_gz, t_gz)


# Part 4: AnnealZOOPs
def m_step_azoops(inputs, k, o):
    """Calculate the current M matrix for AnnealZOOPs algorithm"""
    a = np.repeat(np.array(0.25), k)
    # Initialize M
    M = np.zeros((4, k))
    for j, value in enumerate(a):
        M[:, j] = M[:, j] + value

    all_kmers = []

    for i, inp in enumerate(inputs):
        current_o = o[i]

        if current_o != -1:
            all_kmers.append(inp[current_o : current_o + k])
        else:
            continue

    # Go through each kmer and update M matrix accordingly
    for kmer in all_kmers:
        for j, i in enumerate(kmer):
            M[i - 1, j] = M[i - 1, j] + 1

    # Normalize
    column_sums = M.sum(axis=0)

    normalized_M = np.divide(M, column_sums)

    return normalized_M


def e_step_azoops(inputs, M, k, T):
    """E step for AnnealZOOPs algorithm"""
    offsets = []
    # r_o = 1
    probs = []

    for inp in inputs:
        o_min = 0
        o_max = max_offset(inp, k)
        # o_best = 0
        # p_q_best = 0
        current_p_qs = [1]

        possible_o = [-1]
        possible_o.extend(range(o_min, o_max + 1))

        for o in range(o_min, o_max + 1):
            current_kmer = inp[o : o + k]
            current_p = 1
            current_q = 1

            # Calculate current P
            for j, i in enumerate(current_kmer):
                current_p = current_p * M[i - 1, j]

            # Calculate current Q
            current_q = 0.25 ** k

            p_q = current_p / current_q

            current_p_qs.append(p_q)

        # Gibbs sampling with simulated annealing
        # T is current tempetature
        current_p_qs = np.array(current_p_qs)
        current_p_qs = current_p_qs ** T

        norm = np.sum(current_p_qs)

        normalized_p_qs = current_p_qs / norm

        o_sampled = gibbs_sample(possible_o, normalized_p_qs)

        probs.append(current_p_qs[o_sampled + 1])
        offsets.append(o_sampled)

    # Calculate E(m, o)
    probs = np.array(probs)
    e_m = np.log10(probs).sum()

    return e_m, offsets


def azoops(inputs, k):
    """AnnealZOOPs algorithm"""
    # Convert the inputs to a list of sequences
    # inputs = convert_text_to_list(inputs_path)
    converted_inputs = []
    e_m_log = []

    for inp in inputs:
        curr_converted_inp = convert_seq(inp)
        converted_inputs.append(curr_converted_inp)

    # Hardcoded for now
    t_max = 1000
    o_init = np.repeat(np.array(0), len(inputs))
    o_current = np.copy(o_init)
    # e_m_old = 0
    e_m_current = 0
    # e_m_best = 0
    iteration = 0

    time_start = time.time()

    for i in range(t_max):
        t_current = iteration / (t_max - 1)

        M_current = m_step_azoops(converted_inputs, k, o_current)

        e_m_current, o_current = e_step_azoops(
            converted_inputs, M_current, k, t_current
        )

        iteration += 1

        e_m_log.append(e_m_current)

    time_stop = time.time()

    time_complete = time_stop - time_start

    # Add 1 to final returned offsets since Python uses 0-indexing
    o_final = [value + 1 for value in o_current]

    return e_m_current, o_final, M_current, time_complete, e_m_log


e_m_az, o_az, M_az, t_az, azoops_log = azoops(inputs_list, 6)
# print(e_m_az, o_az, M_az, t_az)

# Sample data
plot_result(hardoops_log, "Sample Input: HardOOPs", "hardoops_sample")
plot_result(hzoops_log, "Sample Input: HardZOOPs", "hardzoops_sample")
plot_result(gzoops_log, "Sample Input: GibbsZOOPs", "gibbszoops_sample")
plot_result(azoops_log, "Sample Input: AnnealZOOPs", "annealzoops_sample")

write_file("hardoops_sample", t, e_m, o, M)
write_file("hardzoops_sample", t_hz, e_m_hz, o_hz, M_hz)
write_file("gibbszoops_sample", t_gz, e_m_gz, o_gz, M_gz)
write_file("annealzoops_sample", t_az, e_m_az, o_az, M_az)

# Load inputs
inputs_1 = convert_text_to_list(
    "/home/matt/Dev/acb/part_2/assignment_1/test_input_1.txt"
)
inputs_2 = convert_text_to_list(
    "/home/matt/Dev/acb/part_2/assignment_1/test_input_2.txt"
)
inputs_3 = convert_text_to_list(
    "/home/matt/Dev/acb/part_2/assignment_1/test_input_3.txt"
)
inputs_4 = convert_text_to_list(
    "/home/matt/Dev/acb/part_2/assignment_1/test_input_4.txt"
)
inputs_5 = convert_text_to_list(
    "/home/matt/Dev/acb/part_2/assignment_1/test_input_5.txt"
)

# Test input 1
e_m_1, o_1, M_1, t_1, hardoops_log_1 = hardoops(inputs_1, 6)
e_m_hz_1, o_hz_1, M_hz_1, t_hz_1, hzoops_log_1 = hzoops(inputs_1, 6)
e_m_gz_1, o_gz_1, M_gz_1, t_gz_1, gzoops_log_1 = gzoops(inputs_1, 6)
e_m_az_1, o_az_1, M_az_1, t_az_1, azoops_log_1 = azoops(inputs_1, 6)

plot_result(hardoops_log_1, "Test Input 1: HardOOPs", "hardoops_test_1")
plot_result(hzoops_log_1, "Test Input 1: HardZOOPs", "hardzoops_test_1")
plot_result(gzoops_log_1, "Test Input 1: GibbsZOOPs", "gibbszoops_test_1")
plot_result(azoops_log_1, "Test Input 1: AnnealZOOPs", "annealzoops_test_1")

write_file("hardoops_test_1", t_1, e_m_1, o_1, M_1)
write_file("hardzoops_test_1", t_hz_1, e_m_hz_1, o_hz_1, M_hz_1)
write_file("gibbszoops_test_1", t_gz_1, e_m_gz_1, o_gz_1, M_gz_1)
write_file("annealzoops_test_1", t_az_1, e_m_az_1, o_az_1, M_az_1)

# Test input 2
e_m_2, o_2, M_2, t_2, hardoops_log_2 = hardoops(inputs_2, 6)
e_m_hz_2, o_hz_2, M_hz_2, t_hz_2, hzoops_log_2 = hzoops(inputs_2, 6)
e_m_gz_2, o_gz_2, M_gz_2, t_gz_2, gzoops_log_2 = gzoops(inputs_2, 6)
e_m_az_2, o_az_2, M_az_2, t_az_2, azoops_log_2 = azoops(inputs_2, 6)

plot_result(hardoops_log_2, "Test Input 2: HardOOPs", "hardoops_test_2")
plot_result(hzoops_log_2, "Test Input 2: HardZOOPs", "hardzoops_test_2")
plot_result(gzoops_log_2, "Test Input 2: GibbsZOOPs", "gibbszoops_test_2")
plot_result(azoops_log_2, "Test Input 2: AnnealZOOPs", "annealzoops_test_2")

write_file("hardoops_test_2", t_2, e_m_2, o_2, M_2)
write_file("hardzoops_test_2", t_hz_2, e_m_hz_2, o_hz_2, M_hz_2)
write_file("gibbszoops_test_2", t_gz_2, e_m_gz_2, o_gz_2, M_gz_2)
write_file("annealzoops_test_2", t_az_2, e_m_az_2, o_az_2, M_az_2)

# Test input 3
e_m_3, o_3, M_3, t_3, hardoops_log_3 = hardoops(inputs_3, 6)
e_m_hz_3, o_hz_3, M_hz_3, t_hz_3, hzoops_log_3 = hzoops(inputs_3, 6)
e_m_gz_3, o_gz_3, M_gz_3, t_gz_3, gzoops_log_3 = gzoops(inputs_3, 6)
e_m_az_3, o_az_3, M_az_3, t_az_3, azoops_log_3 = azoops(inputs_3, 6)

plot_result(hardoops_log_3, "Test Input 3: HardOOPs", "hardoops_test_3")
plot_result(hzoops_log_3, "Test Input 3: HardZOOPs", "hardzoops_test_3")
plot_result(gzoops_log_3, "Test Input 3: GibbsZOOPs", "gibbszoops_test_3")
plot_result(azoops_log_3, "Test Input 3: AnnealZOOPs", "annealzoops_test_3")

write_file("hardoops_test_3", t_3, e_m_3, o_3, M_3)
write_file("hardzoops_test_3", t_hz_3, e_m_hz_3, o_hz_3, M_hz_3)
write_file("gibbszoops_test_3", t_gz_3, e_m_gz_3, o_gz_3, M_gz_3)
write_file("annealzoops_test_3", t_az_3, e_m_az_3, o_az_3, M_az_3)

# Test input 4
e_m_4, o_4, M_4, t_4, hardoops_log_4 = hardoops(inputs_4, 6)
e_m_hz_4, o_hz_4, M_hz_4, t_hz_4, hzoops_log_4 = hzoops(inputs_4, 6)
e_m_gz_4, o_gz_4, M_gz_4, t_gz_4, gzoops_log_4 = gzoops(inputs_4, 6)
e_m_az_4, o_az_4, M_az_4, t_az_4, azoops_log_4 = azoops(inputs_4, 6)

plot_result(hardoops_log_4, "Test Input 4: HardOOPs", "hardoops_test_4")
plot_result(hzoops_log_4, "Test Input 4: HardZOOPs", "hardzoops_test_4")
plot_result(gzoops_log_4, "Test Input 4: GibbsZOOPs", "gibbszoops_test_4")
plot_result(azoops_log_4, "Test Input 4: AnnealZOOPs", "annealzoops_test_4")

write_file("hardoops_test_4", t_4, e_m_4, o_4, M_4)
write_file("hardzoops_test_4", t_hz_4, e_m_hz_4, o_hz_4, M_hz_4)
write_file("gibbszoops_test_4", t_gz_4, e_m_gz_4, o_gz_4, M_gz_4)
write_file("annealzoops_test_4", t_az_4, e_m_az_4, o_az_4, M_az_4)

# Test input 5
e_m_5, o_5, M_5, t_5, hardoops_log_5 = hardoops(inputs_5, 6)
e_m_hz_5, o_hz_5, M_hz_5, t_hz_5, hzoops_log_5 = hzoops(inputs_5, 6)
e_m_gz_5, o_gz_5, M_gz_5, t_gz_5, gzoops_log_5 = gzoops(inputs_5, 6)
e_m_az_5, o_az_5, M_az_5, t_az_5, azoops_log_5 = azoops(inputs_5, 6)

plot_result(hardoops_log_5, "Test Input 5: HardOOPs", "hardoops_test_5")
plot_result(hzoops_log_5, "Test Input 5: HardZOOPs", "hardzoops_test_5")
plot_result(gzoops_log_5, "Test Input 5: GibbsZOOPs", "gibbszoops_test_5")
plot_result(azoops_log_5, "Test Input 5: AnnealZOOPs", "annealzoops_test_5")

write_file("hardoops_test_5", t_5, e_m_5, o_5, M_5)
write_file("hardzoops_test_5", t_hz_5, e_m_hz_5, o_hz_5, M_hz_5)
write_file("gibbszoops_test_5", t_gz_5, e_m_gz_5, o_gz_5, M_gz_5)
write_file("annealzoops_test_5", t_az_5, e_m_az_5, o_az_5, M_az_5)
