# Advanced Computational Biology 

# Assignment 2

# Matthew C. McFee

Requirements: 
Python (3.8.5), numpy (1.19.4), biopython (1.78), scipy (1.5.3)

Instructions:

Cd into this folder and run

python assignment_2_part_1.py

python assignment_2_part_2.py

Alternatively either of the Jupyter notebooks in the folder can be opened and all cells
run

The output of part 1's code  is in the folder part_1_txt

The CSVs for each random seed clustering for part 2 are in the folder part_2_csvs. The
seed used is in the CSV file name and the cluster assignments are in the column titled
"Cluster"

The cluster sizes for each seed are contained in a text file in the folder part_2_txt

The enriched and depleted genes of the clusters are int he enriched_and_depleted.json

The adjusted p-values for part 2 are saved in the file adjusted_p_values.csv

Both the original data and the data with assigned clusters are contained in the CSV
files starting with "Baise"