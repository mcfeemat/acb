#!/usr/bin/env python
# coding: utf-8
"""
Author: Matthew C. McFee

Description: A basic implementation of k-means clustering algorithm to analyze data from
Baise et al. 2014.
"""

import numpy as np
import pandas as pd
import json

from scipy.stats import mannwhitneyu

np.random.seed(5)


class KMeans:
    """
    A simple implementation of the K-means clustering algorithm using
    the Python standard library and numpy. Initialization is random and
    the algorithm terminates when max iterations has been met or the centroids
    stop updating.
    """

    def __init__(self, data, labels, k, seed, rescale=False):
        self.data = data
        self.labels = labels
        self.k = k
        self.seed = seed
        self.means = np.zeros((self.k, self.data.shape[1]))
        self.old_means = np.zeros((self.k, self.data.shape[1]))
        self.distances = np.zeros((data.shape[0], self.k))
        self.assigned_clusters = np.zeros(data.shape[0])
        self.rescale = rescale

        # Set random seed
        np.random.seed(self.seed)
        # random_check = np.random.randint(1000, size=10)
        # print(random_check)

    def rescale_data(self):
        column_means = self.data.mean(axis=0)
        column_stds = self.data.std(axis=0)

        self.data = self.data - column_means
        self.data = self.data / column_stds

    def initialize_clusters(self):
        num_rows = self.data.shape[0]
        rand_indices = np.random.choice(num_rows, size=self.k, replace=False)

        for i, ind in enumerate(rand_indices):
            mean = self.data[ind]
            self.means[i] = mean

    def calculate_distances(self):
        for i in range(self.distances.shape[0]):
            current_diff = self.data[i] - self.means
            current_distances = np.linalg.norm(current_diff, axis=1)
            self.distances[i] = current_distances

    def update_assigned_clusters(self):
        for i in range(self.distances.shape[0]):
            current_distances = self.distances[i]
            closest_cluster = np.argmin(current_distances)
            self.assigned_clusters[i] = closest_cluster

    def update_cluster_means(self):
        for i in range(self.k):
            indices = self.assigned_clusters == i
            indices = np.nonzero(indices)

            filtered = self.data[indices]

            new_means = np.mean(filtered, axis=0)

            self.means[i] = new_means

    def correct_labels(self):
        counter = 0
        for i in range(self.data.shape[0]):
            if self.labels[i] == self.assigned_clusters[i]:
                counter += 1

        return 100 * (counter / self.data.shape[0])

    def predict(self, test_points):
        prediction_distances = np.zeros((test_points.shape[0], self.k))
        predictions = np.zeros(test_points.shape[0])

        for i in range(test_points.shape[0]):
            current_diff = test_points[i] - self.means
            current_distances = np.linalg.norm(current_diff, axis=1)
            prediction_distances[i] = current_distances

        for i in range(test_points.shape[0]):
            current_distances = prediction_distances[i]
            closest_cluster = np.argmin(current_distances)
            predictions[i] = closest_cluster

        return predictions

    def fit(self, n):
        if self.rescale:
            self.rescale_data()

        self.initialize_clusters()

        for i in range(n):
            self.calculate_distances()

            self.update_assigned_clusters()

            self.old_means = np.copy(self.means)

            self.update_cluster_means()

            if np.array_equal(self.means, self.old_means):
                self.n_iters = i
                break


# Test case taken from https://scikit-learn.org/stable/modules/generated/sklearn.cluster.KMeans.html
# X = np.array([[1, 2], [1, 4], [1, 0], [10, 2], [10, 4], [10, 0]])
# L = None

# km = KMeans(X, None, 2, 0)
# km.fit(100)
# print(km.means)


# This portion of the code was simply to generate some toy datasets to verify that my
# k-means can converge to the same solution (same centroids) as sklearn's implementation
# of k-means initializing centroids by randomly select data points
# from sklearn.cluster import KMeans as skkm

# skkm_test = skkm(n_clusters=2, random_state=0).fit(km.data)

# test_points = np.array([[0, 0], [12, 3]])
# predictions = km.predict(test_points)

# X = -2 * np.random.rand(100,2)
# X1 = 1 + 2 * np.random.rand(50,2)
# X[50:100, :] = X1

# test = skkm(n_clusters=2).fit(X)

# my_method = KMeans(X, None, 2, 0, rescale=False)
# my_method.fit(100)


# Part 2 question 1
df = pd.read_csv("Biase_2014.csv", header=0)


def get_cluster_sizes(assigned_clusters, k):
    """
    Find the sizes of each cluster from a list of all cluster assignments
    """
    sizes = []

    for i in range(k):
        members = np.where(assigned_clusters == i)
        count = np.count_nonzero(members)
        sizes.append(count)

    return sizes


def append_clusters(df, predictions):
    """
    Append cluster predictions to the original csv file
    """
    df["Cluster"] = predictions.tolist()

    return df


# Select a set of seeds (initial seed was set to 5)
random_seeds = np.random.randint(100, size=20)

# Skip the gene name column
X = df.iloc[:, 1:].values
means = X.mean(axis=0)
std = X.std(axis=0)

# Standardization of data by subtracting column means and dividing by column std
# Data must be transposed since we want to cluster the samples not the genes
X = (X - means) / std
X = X.T

cluster_sizes = []

# Cluster the data using 20 random cluster initializations
for seed in random_seeds:
    km = KMeans(X, None, 4, seed)
    km.fit(1000)

    predictions = km.assigned_clusters

    current_cluster_sizes = get_cluster_sizes(predictions, 4)
    cluster_sizes.append(current_cluster_sizes)

    df_current = pd.DataFrame(
        km.data, index=df.columns[1:].tolist(), columns=df.gene.tolist()
    )
    df_current = append_clusters(df_current, predictions)

    file_name = "Baise_2014_clustered_random_seed_" + str(seed) + ".csv"
    df_current.to_csv(file_name)

with open("cluster_sizes.txt", "w") as file:
    for i, seed in enumerate(random_seeds):
        file.write("random seed: " + str(seed) + "\n")
        file.write(str(cluster_sizes[i]) + "\n")

df_stats = pd.read_csv("Baise_2014_clustered_random_seed_0.csv", header=0, index_col=0)

# As discussed in office hours my initial ideas was to find enriched and depleted genes
# in each cluster by finding genes with values above or below the mean (positive values
# for enriched and negative values for depleted
# def find_enriched_and_depleted(df, cluster_id):
#    enriched = []
#    depleted = []
#
#    cluster = df[df.Cluster == cluster_id]
#
#    for gene in df.columns:
#        values = df[gene].values
#        avg = np.mean(values)
#
#        if avg >= 0:
#            enriched.append(gene)
#        else:
#            depleted.append(gene)
#
#    return enriched, depleted
#
# enriched_c0, depleted_c0 = find_enriched_and_depleted(df_stats, 0)

# def find_p_values(df, gene_list, cluster_id, crit_value):
#    gene_p_values = {}
#
#    cluster_data = df[df.Cluster == cluster_id]
#    other_ids = np.unique(df.Cluster).tolist()
#
#    other_ids.remove(cluster_id)
#
#    for gene in gene_list:
#        x = cluster_data[gene].values
#        entries = []
#
#        for id in other_ids:
#            other_data = df[df.Cluster == id]
#            y = other_data[gene].values
#
#            _, p_value = mannwhitneyu(x, y, alternative="two-sided")
#            # thresh = crit_value / len(other_ids)
#
#            # Apply Bonferroni correction
#            p_value = len(other_ids) * p_value
#
#            if p_value < crit_value:
#                sig = "significant"
#
#            else:
#                sig = "non-significant"
#
#            entries.append([id, p_value, sig])
#
#        gene_p_values[gene] = entries
#
#    return gene_p_values


def find_gene_p_values(df, k):
    """
    Find enriched and depleted genes by comparing distribution in each cluster to the
    others. P-values are adjusted for multiple comparisons using the Bonferonni
    correction.
    """
    p_values = np.zeros((df.shape[1], k))

    genes_list = df.columns

    cluster_ids = range(k)

    for i, gene in enumerate(genes_list):
        current_p_values = []
        for cluster_id in cluster_ids:
            df_cluster = df[df.Cluster == cluster_id]
            df_rest = df[df.Cluster != cluster_id]

            x = df_cluster[gene].values
            y = df_rest[gene].values

            _, p = mannwhitneyu(x, y, alternative="two-sided")

            current_p_values.append(p)

        current_p_values = np.array(current_p_values)

        p_values[i] = current_p_values

    #  Bonferonni correction by multiplying p-values by number of comparisons
    p_values = k * p_values

    df_p_values = pd.DataFrame(p_values, columns=range(k), index=genes_list)

    return df_p_values, p_values


df_p_values, p_values = find_gene_p_values(df_stats, 4)


def find_enriched_and_depleted(df_p_values, genes_list, crit_p):
    """
    Enriched and depleted genes are those that have a significant p-value given the
    assignments criteria.
    """
    columns = df_p_values.columns
    cluster_dict = {}

    for column in columns:
        p_values = df_p_values[column].values
        enriched_and_depleted = []

        for i, p in enumerate(p_values):
            if p < crit_p:
                enriched_and_depleted.append(genes_list[i])

        cluster_dict[column] = enriched_and_depleted

    return cluster_dict


def find_genes_in_common(cluster_dict, cluster):
    """
    Find the genes that are enriched and depleted in both the selected cluster and the
    other 3 clusters.
    """
    cluster_genes = cluster_dict[cluster]
    cluster_genes_set = set(cluster_genes)

    other_clusters = list(cluster_dict.keys())
    other_clusters.remove(cluster)

    result = {}

    for other_cluster in other_clusters:
        other_genes = cluster_dict[other_cluster]
        other_genes = set(other_genes)

        number_in_common = len(cluster_genes_set.intersection(other_genes))

        result[other_cluster] = number_in_common

    return result


# Save the calculated adjusted p-values in a csv file for easy viewing
df_p_values = df_p_values.drop(labels="Cluster", axis=0)
df_p_values.to_csv("adjusted_p_values.csv")

enriched_and_depleted = find_enriched_and_depleted(df_p_values, df_stats.columns, 0.05)

in_common = find_genes_in_common(enriched_and_depleted, 3)

print(
    "The number of enriched/depleted genes in common with the four cell enriched cluster 3: ",
    in_common,
)

# Save lists of enriched and depleted genes in each of the clusters to a human readable format
enriched_and_depleted_file = open("enriched_and_depleted.json", "w")

json.dump(enriched_and_depleted, enriched_and_depleted_file)

enriched_and_depleted_file.close()
