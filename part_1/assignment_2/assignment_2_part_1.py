#!/usr/bin/env python
# coding: utf-8
"""
Author: Matthew C. McFee

Description: A simple Python implementation of the Smith-Waterman algorithm. This
implementation discards alignments with multiple gaps. This implementation finds DNA and
RNA complementary alignments but can be adapted to traditional RNA-RNA alignments etc.
"""

import numpy as np
from Bio import SeqIO
import sys
import re

# Prevent cropping of matrices when printed to console
np.set_printoptions(threshold=sys.maxsize)

scores = {"GC": 3, "CG": 3, "AU": 2, "UA": 2, "GU": 1, "UG": 1, "GAP": -1, "OTHER": -1}
good_keys = ["GC", "CG", "AU", "UA", "GU", "UG"]

sequences = []

for record in SeqIO.parse("sample_input.fasta", "fasta"):
    sequences.append(str(record.seq))

sequences[1] = sequences[1].replace("#Testcase2", "")

pair_1 = [sequences[0], sequences[1][::-1]]
pair_2 = [sequences[2], sequences[3][::-1]]


def get_score_matrix(query_target_pair):
    """
    Generate the score matrix. Definition of algorithm taken from course notes
    """
    # Assume query is first entry and target is the second
    query = query_target_pair[0]
    target = query_target_pair[1]

    score_matrix = np.zeros((len(target) + 1, len(query) + 1))
    direction_matrix = np.full(
        (len(target) + 1, len(query) + 1), ["null"], dtype=object
    )

    for i in range(1, len(target) + 1):
        for j in range(1, len(query) + 1):
            pairing = query[j - 1] + target[i - 1]

            if pairing not in good_keys:
                pairing = "OTHER"

            moves = {
                "new": scores[pairing],
                "diag": score_matrix[i - 1, j - 1] + scores[pairing],
                "down": score_matrix[i - 1, j] + scores["GAP"],
                "right": score_matrix[i, j - 1] + scores["GAP"],
                "null": 0,
            }

            best_value = max(moves.values())
            best_moves = []

            for key, value in moves.items():
                if value == best_value:
                    best_moves.append(key)

            score_matrix[i, j] = best_value
            direction_matrix[i, j] = best_moves

    return score_matrix, direction_matrix


score_matrix, direction_matrix = get_score_matrix(pair_1)


def get_source(direction_matrix, i, j, position_lists, current_list):
    """
    Recursively move back through the score matrix usingt he direction matrix which
    specifies the sources of any given point (i, j)s score
    """
    current_directions = direction_matrix[i, j]

    current_list.append((i, j))

    for direction in current_directions:
        if direction == "diag":
            i = i - 1
            j = j - 1

        elif direction == "down":
            i = i - 1
            j = j

        elif direction == "right":
            i = i
            j = j - 1

        elif direction == "new":
            i = i
            j = j
            position_lists.append(current_list)
            continue

        else:
            break

        get_source(direction_matrix, i, j, position_lists, current_list.copy())

    return position_lists


def get_symbol(query_base, target_base, current_move):
    """
    Determine the query base, target_base, and pairing symbol for each element of the
    sequence alignment
    """
    # if query_base + target_base in good_keys and current_move not in ["down", "right"]:
    #    return "|", query_base, target_base
    #
    # else:
    #    return " ", query_base, target_base
    if query_base + target_base in good_keys:
        match = True
    else:
        match = False

    if match:
        return "|", query_base, target_base

    else:
        # print("GET SYMBOL", current_move)
        if current_move == "down":
            return " ", "-", target_base

        elif current_move == "right":
            return " ", query_base, "-"

        else:
            return " ", query_base, target_base


def get_alignment(query, target, score_matrix, direction_matrix):
    """
    Perform the backtrace to find the alignment
    """
    # Find index of max value in the aray
    max_value = np.amax(score_matrix)
    max_ind = []

    for i in range(0, score_matrix.shape[0]):
        for j in range(0, score_matrix.shape[1]):
            current_val = score_matrix[i, j]

            if current_val == max_value:
                max_ind.append([i, j])

    starting_positions = []

    for ind in max_ind:
        position_list = get_source(direction_matrix, ind[0], ind[1], [], [])
        starting_positions.append(position_list)

    result = []

    for i, ind in enumerate(max_ind):
        best_score = score_matrix[ind[0], ind[1]]

        starting_position = starting_positions[i]

        for path in starting_position:
            end = True
            current_query = []
            current_symbols = []
            current_target = []

            for i, pair in enumerate(path):
                if end:
                    curr_query = query[pair[1] - 1]
                    curr_target = target[pair[0] - 1]

                    if curr_query + curr_target not in good_keys:
                        current_symbols.append(" ")
                        current_query.append(curr_query)
                        current_target.append(curr_target)
                    else:
                        current_symbols.append("|")
                        current_query.append(curr_query)
                        current_target.append(curr_target)

                    end = False

                    continue

                if i == len(path) - 1:
                    next_move = path[i]

                else:
                    next_move = path[i + 1]

                if pair[0] == next_move[0] + 1 and pair[1] == next_move[1] + 1:
                    move = "diag"

                elif pair[0] == next_move[0] + 1 and pair[1] == next_move[1]:
                    move = "down"

                elif pair[0] == next_move[0] and pair[1] == next_move[1] + 1:
                    move = "right"

                else:
                    move = None

                query_base = query[pair[1] - 1]
                target_base = target[pair[0] - 1]

                symbol, query_base, target_base = get_symbol(
                    query_base, target_base, move
                )

                current_symbols.append(symbol)
                current_query.append(query_base)
                current_target.append(target_base)

            new_result = [
                current_query,
                current_symbols,
                current_target,
                ind,
                best_score,
            ]

            result.append(new_result)

    return result


result = get_alignment(pair_1[0], pair_1[1], score_matrix, direction_matrix)

score_matrix_2, direction_matrix_2 = get_score_matrix(pair_2)
result_2 = get_alignment(pair_2[0], pair_2[1], score_matrix_2, direction_matrix_2)


def prettify_alignment(alignment):
    """
    Prettify the alignments generate so they display like the alignments in the sample
    files
    """
    query = "".join(alignment[0])
    symbols = "".join(alignment[1])
    target = "".join(alignment[2])

    query = "3' " + query + " 5'"
    target = "5' " + target + " 3'"
    symbols = "   " + symbols + "   "

    print(query)
    print(symbols)
    print(target)

    return query, symbols, target


def save_alignments(alignments, title, file_name):
    """
    Save all generated alignments into a .txt file
    """
    with open(file_name, "w") as file:
        for i, alignment in enumerate(alignments):

            # Write only on first iteration
            if i == 0:
                file.write(title + "\n")
                file.write("Best score: " + str(alignment[-1]) + "\n")

            query, symbols, target = prettify_alignment(alignment)

            regex = r"-{2,}"
            match_multi_gap_query = re.search(regex, query)
            match_multi_gap_target = re.search(regex, target)

            # Discard multi-gap alignments
            if match_multi_gap_query or match_multi_gap_target:
                continue

            file.write(
                "Starting position in score matrix: " + str(alignment[-2]) + "\n"
            )

            file.write(query + "\n")
            file.write(symbols + "\n")
            file.write(target + "\n")

    return None


save_alignments(result, "Test case #1", "output-sample-case-1.txt")

save_alignments(result_2, "Test case #2", "output-sample-case-2.txt")

sequences_test = []

for record in SeqIO.parse("test_input.fasta", "fasta"):
    sequences_test.append(str(record.seq))

pairs_test = []

# Get the alignments for the test cases with no solutions provided by the TA
for i in range(0, len(sequences_test), 2):
    current_pair = [sequences_test[i], sequences_test[i + 1][::-1]]
    pairs_test.append(current_pair)

for i, pair in enumerate(pairs_test):
    score_matrix, direction_matrix = get_score_matrix(pair)
    result = get_alignment(pair[0], pair[1], score_matrix, direction_matrix)

    title = "Test case #" + str(i + 1)
    file_name = "output-test-case-" + str(i + 1) + ".txt"

    save_alignments(result, title, file_name)
