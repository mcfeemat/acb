# Advanced Computational Biology 

# Assignment 3

# Matthew C. McFee

Requirements: 
Python (3.8.5), numpy (1.19.4), biopython (1.78), scipy (1.5.3)

Instructions:

All of the code has already been run and assigment_3.pdf specifies which CSV or txt file
contains the output of the code for each part of the assignment.

To rerun the code

cd into the directory and run the command

python assignment_3.py

A jupyter notebook is also available.