#!/usr/bin/env python
# coding: utf-8
"""
Author: Matthew McFee

Description: ACB Assignment 3
"""

import numpy as np
import pandas as pd
import os
import scipy.stats as stats

# Part 1: Create a single CSV file with every VAF variant observed in the control samples.
# The format should be a column containing every genome position and variant and all other columns
# being the VAF of that entry in each given control sample. If a genome position + variant is not
# in a sample set it to 0.

control_files = os.listdir("controls")
control_files.sort(key=lambda x: int(x.strip("control").strip(".csv")))

df = pd.read_csv("controls/control13.csv", header=0)

cols = ["chr", "pos", "nucleotide"]
df["variant"] = df[cols].apply(lambda row: " ".join(row.values.astype(str)), axis=1)

selected_cols = ["variant", "VAF"]

df = df[selected_cols]

df = df.rename(columns={"VAF": "control13"})


def clean_csv(df, col_name):
    """
    Clean CSV files to be in the correct format as specified in the assignment.
    """
    cols = ["chr", "pos", "nucleotide"]
    df["variant"] = df[cols].apply(lambda row: " ".join(row.values.astype(str)), axis=1)

    selected_cols = ["variant", "VAF"]

    df = df[selected_cols]

    df = df.rename(columns={"VAF": col_name})

    return df


def merge_control_csvs(csvs_list, root_dir):
    """
    Merge the control CSVs into a single file where NaNs are set to 0.
    """
    path = os.path.join(root_dir, csvs_list[0])
    df_main = pd.read_csv(path, header=0)
    col_name = csvs_list[0].replace(".csv", "")
    df_main = clean_csv(df_main, col_name)

    for csv in csvs_list[1:]:
        path = os.path.join(root_dir, csv)

        df_current = pd.read_csv(path, header=0)

        col_name_current = csv.replace(".csv", "")
        df_current = clean_csv(df_current, col_name_current)

        df_main = df_main.merge(df_current, how="outer", on="variant")

    # Replace NaN with 0
    df_main = df_main.fillna(0)

    return df_main


df_merged = merge_control_csvs(control_files, "controls")
df_merged.to_csv("control_vaf.csv", index=False)

# Part 2 fit the exponential distribution to each variant
# test_data = df_merged.iloc[5, 1:].values[0]
# print(type(test_data))


def get_rates(values):
    """
    Calculate rates of exponential distributions fit to each variant in the controls.
    This is done by fitting the values in each variant row to an exponential distribution.
    The loc parameter is set to 0 and the distribution is fit by adjusting the parameter
    scale. Scale is 1/lamdba or lambda = 1/scale.
    """
    rates = []

    for i in range(values.shape[0]):
        log, scale = stats.expon.fit(values[i], floc=0)
        lambda_val = 1 / scale

        rates.append(lambda_val)

    return rates


input_data = df_merged.iloc[:, 1:].values
rates = get_rates(input_data)

df_rates = df_merged.copy()
df_rates["rate"] = rates
df_rates.to_csv("control_vaf_rate.csv", index=False)

# Test selecting the rate correctly
variant = "chr17 58740887 A"

df_rates[df_rates.variant == variant]["rate"].values[0]

# Part 3
# Calculate 1 - CDF(x) where x is the patient value and the CDF is
# from the fit of the data for that variant(controls)


def get_enrichment_p_values(df_patient, df_controls, adjust_values=False):
    """
    Calculate the p-values of the variant values for each patient. Cumulative
    distribution functions for each variant's control data can be generated using
    the previously calculated rates. The variants CDF is then evaluated at the
    patients varient expression level. The probability of obtaining a patient
    expression value greater than the recorded value is then 1 - CDF(patient data).
    """
    p_values = []

    patient_values = df_patient[["chr", "pos", "nucleotide", "VAF"]].values

    for i in range(patient_values.shape[0]):
        variant = (
            patient_values[i][0]
            + " "
            + str(patient_values[i][1])
            + " "
            + patient_values[i][2]
        )

        lambda_val = df_controls[df_controls.variant == variant]["rate"].values
        scale_val = 1 / lambda_val
        patient_val = patient_values[i][-1]

        # Print Tests
        # print(variant)
        # print(lambda_val)
        # print(scale_val)
        # print(patient_val)

        p_value = 1 - stats.expon.cdf(patient_val, loc=0, scale=scale_val)

        p_values.append(p_value)

    if adjust_values:
        num_comparisons = patient_values.shape[0]

        p_values = num_comparisons * np.array(p_vales)

    else:
        p_values = np.array(p_values)

    return p_values


# Test above function
df_test_patient = pd.read_csv("patients/Patient_0107.csv", header=0)
p_values_test = get_enrichment_p_values(df_test_patient, df_rates)


def get_patient_p_values(
    patient_files, root_dir, target_dir, df_controls, adjust=False
):
    """
    Calculate appropriate p-values and append to patient file and save.
    """
    for patient_file in patient_files:
        path = os.path.join(root_dir, patient_file)
        df_current_patient = pd.read_csv(path, header=0)

        current_p_values = get_enrichment_p_values(
            df_current_patient, df_controls, adjust_values=adjust
        )

        df_pvs = df_current_patient.copy()
        df_pvs["p"] = current_p_values
        save_path = os.path.join(target_dir, patient_file)
        df_pvs.to_csv(save_path, index=False)

    return None


patient_files = os.listdir("patients")

get_patient_p_values(
    patient_files, "patients", "patients_with_stats", df_rates, adjust=False
)

patient_files[0]

# Part 4: Need to merge everything, multiply the p-values by total number of
# comparisons then record each unique variant, the samples that had a significant
# p-value
df_merged_stats = pd.read_csv("patients_with_stats/Patient_2819.csv", header=0)
df_merged_stats["sample"] = "Patient_2819.csv"

for patient_file in patient_files[1:]:
    path = os.path.join("patients_with_stats", patient_file)

    df_current = pd.read_csv(path, header=0)
    df_current["sample"] = patient_file

    df_merged_stats = df_merged_stats.append(df_current)

cols = ["chr", "pos", "nucleotide"]
df_merged_stats["variant"] = df_merged_stats[cols].apply(
    lambda row: " ".join(row.values.astype(str)), axis=1
)

df_merged_stats = df_merged_stats[
    ["variant", "sample", "count", "coverage_at_position", "VAF", "p"]
]

# df_merged_stats

corrected_ps = df_merged_stats.shape[0] * df_merged_stats.iloc[:, -1].values

df_merged_stats["p.corrected"] = corrected_ps

# df_merged_stats

unique_variants = np.unique(df_merged_stats.variant)

unique_variants

df_sig = df_merged_stats[df_merged_stats["p.corrected"] <= 0.05]

df_sig = df_sig[["variant", "sample", "p", "p.corrected"]]
df_sig["sample"] = df_sig["sample"].str.replace(".csv", "")

# df_sig

all_variants = df_sig["variant"].values.tolist()

all_variants

all_rates = []

for variant in all_variants:
    rate = df_rates[df_rates.variant == variant].values.tolist()[0][-1]
    all_rates.append(rate)

df_sig["rate"] = all_rates

df_sig.to_csv("significant_variants.csv", index=False)

# Part 5: Generation of a contigency table
df_mutations = pd.read_csv("mutations.csv", header=0)

cols = ["chr", "pos", "alt"]
df_mutations["variant"] = df_mutations[cols].apply(
    lambda row: " ".join(row.values.astype(str)), axis=1
)
gene_set = df_mutations[["variant", "sample"]].values.tolist()

gene_set_fixed = []

for gene in gene_set:
    gene_set_fixed.append(gene[0] + " " + gene[1])

genes_sig = all_variants = df_sig[["variant", "sample"]].values.tolist()

genes_sig_fixed = []

for gene in genes_sig:
    genes_sig_fixed.append(gene[0] + " " + gene[1])

# Find number of significantly above background that are in the gene set
sig_set = set(genes_sig_fixed)
sig_in_gene_set = len(sig_set.intersection(set(gene_set_fixed)))

sig_not_gene_set = len(sig_set) - sig_in_gene_set

# print(sig_in_gene_set)
# print(sig_not_gene_set)

df_non_sig = df_merged_stats[df_merged_stats["p.corrected"] > 0.05]
df_non_sig = df_non_sig[["variant", "sample", "p", "p.corrected"]]
df_non_sig.to_csv("non_significant_variants.csv", index=False)

genes_non_sig = df_non_sig[["variant", "sample"]].values.tolist()

genes_non_sig_fixed = []

for gene in genes_non_sig:
    genes_non_sig_fixed.append(gene[0] + " " + gene[1])

genes_non_sig_set = set(genes_non_sig_fixed)

non_sig_in_gene_set = len(genes_non_sig_set.intersection(set(gene_set_fixed)))
non_sig_not_gene_set = len(genes_non_sig) - non_sig_in_gene_set

# print(non_sig_in_gene_set)
# print(non_sig_not_gene_set)

contingency = np.array([[20, 0], [5, 480]])

_, fisher_p = stats.fisher_exact(contingency)

np.savetxt("contingency.txt", contingency)

with open("contingency.txt", "a") as file:
    file.write("p-value: " + str(fisher_p))

# Part 6: Looking at the variants detected at diagnosis only
final_variants = []
final_samples = []

for gene in gene_set:
    final_variants.append(gene[0])
    final_samples.append(gene[1])

df_merged_stats["sample"] = df_merged_stats["sample"].str.replace(".csv", "")
df_merged_stats = df_merged_stats.reset_index(drop=True)

# print(len(gene_set))

final_indices = []
for gene in gene_set:
    current_index = df_merged_stats[
        (df_merged_stats["variant"] == gene[0]) & (df_merged_stats["sample"] == gene[1])
    ].index.tolist()
    print(gene)
    print(current_index)
    final_indices.extend(current_index)

# df_merged_stats

df_final = df_merged_stats.iloc[final_indices, :]
df_final = df_final.reset_index(drop=True)

# df_final

df_final["p.corrected"] = df_final.shape[0] * df_final.p.values

# df_final

df_final_non_sig = df_final[df_final["p.corrected"] > 0.05]
df_final_non_sig.to_csv("patients_responded_to_treatment.csv", index=False)

df_final.to_csv("variants_detected_diagnosis.csv", index=False)
