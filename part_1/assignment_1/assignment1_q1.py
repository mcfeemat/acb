#!/usr/bin/env python
# coding: utf-8
"""
Author: Matthew C. McFee
Description: Look for instances of the pattern R-x(2)-[ST]-x-[ST] which
indicates the protein has a site which can bind Cmk2
"""

import re
from Bio import SeqIO

reg_exp = r"R..[TS].[TS]"
match_count = 0
total_count = 0

for record in SeqIO.parse("orf_trans.fasta", "fasta"):
    current_seq = str(record.seq)
    # After office hours I realized I must remove *
    current_seq = current_seq.replace("*", "")

    match = re.search(reg_exp, current_seq)

    total_count += 1

    if match:
        match_count += 1

frequency = match_count / total_count
print(frequency)

with open("assignment1-q1.txt", "w") as file:
    file.write(str(frequency))
