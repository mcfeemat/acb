# Advanced Computational Biology 

# Assignment 1

# Matthew C. McFee

Notes: As discussed in office hours I removed the * characters from each
sequence before processing. Ignore the folder called old-version it contains
mistakes which I corrected after your feedback.

Requirements: 
Python (3.8.5), numpy (1.19.4), biopython (1.78)

Instructions:
cd into the folder assignment_1 and run the following commands

python assignment1_q1.py

python assignment1_q2.py

python assignment1_q4.py

python assignment1_q5.py

python assignment1_q6.py
