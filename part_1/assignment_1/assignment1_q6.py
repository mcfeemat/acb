#!/usr/bin/env python
# coding: utf-8
"""
Author: Matthew C. McFee

Description: Script to find the conditional probabilities of all amino acids
given the previous amino acid is isoleucine or glutamine
"""

import numpy as np
import pickle

amino_acid_codes = "ANDRCQEGHILKMPFSTWYV"

iso = []
glut = []

for amino in amino_acid_codes:
    iso_key = "I" + amino
    glut_key = "Q" + amino

    iso.append(iso_key)
    glut.append(glut_key)

# Load the di-amino frequencies
file = open("di-amino.pkl", "rb")

diamino = pickle.load(file)

iso_cond = []
glut_cond = []

diamino_keys = diamino.keys()

iso_keys = [key for key in diamino_keys if key.startswith("I")]
glut_keys = [key for key in diamino_keys if key.startswith("Q")]

prob_iso_first = 0

for key in iso_keys:
    prob_iso_first += diamino[key]

prob_glut_first = 0

for key in glut_keys:
    prob_glut_first += diamino[key]

for item in iso:
    prob = diamino[item] / prob_iso_first
    iso_cond.append(prob)

for item in glut:
    prob = diamino[item] / prob_glut_first
    glut_cond.append(prob)

print("isoleucine: ", iso_cond)
print("glutamine: ", glut_cond)

with open("assignment1-q6.txt", "w") as file:
    file.write("isoleucine: " + str(iso_cond) + "\n")
    file.write("glutamine: " + str(glut_cond) + "\n")
