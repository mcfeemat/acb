Advanced Computational Biology 

Assignment 1

Matthew C. McFee

Requirements: 
Python (3.8.5), numpy (1.19.4), biopython (1.78)

Instructions:
cd into assignment folder and run the following commands

python assignment1-q1.py

python assignment1-q2.py

python assignment1-q4.py

python assignment1-q5.py

python assignment1-q6.py
