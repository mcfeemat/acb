#!/usr/bin/env python
# coding: utf-8
"""
Author: Matthew C. McFee
Description: Script to account the percentage occurances of each amino acid in
the fasta file for the yeast proteosome
"""

from Bio import SeqIO
import numpy as np
import pickle

amino_acid_codes = "ANDRCQEGHILKMPFSTWYV"

amino_count = {}
total_count = 0

# Initialize the amino acid count dictionary
for amino in amino_acid_codes:
    amino_count[amino] = 0

for record in SeqIO.parse("orf_trans.fasta", "fasta"):
    current_seq = str(record.seq)

    for amino in current_seq:
        # Account for potential characters in file that are not amino acids
        if amino not in amino_acid_codes:
            continue

        total_count += 1
        amino_count[amino] += 1

values = list(amino_count.values())

array = np.array(values)

array = array / total_count

# Save the appropriate amino acids in a dictionary on disk for use in part 6 of
# question 2 of the assignment
amino_for_q6 = {}

amino_for_q6["I"] = amino_count["I"] / total_count
amino_for_q6["Q"] = amino_count["Q"] / total_count

gt = [
    0.055, 0.044, 0.062, 0.059, 0.013, 0.040, 0.066, 0.050, 0.022, 0.065,
    0.095, 0.073, 0.021, 0.044, 0.044, 0.090, 0.059, 0.010, 0.034, 0.054
]

gt = np.array(gt)

# The calculated values are in close agreement to those in the assignment table
print(array)

with open("assignment1-q2.txt", "w") as file:
    file.write(str(array))

error = np.absolute(array - gt)
print(error)

with open("assignment2-q3.txt", "w") as file:
    file.write(str(error))

with open("amino.pkl", "wb") as file:
    pickle.dump(amino_for_q6, file)
