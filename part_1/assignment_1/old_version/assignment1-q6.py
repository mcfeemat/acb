#!/usr/bin/env python
# coding: utf-8
"""
Author: Matthew C. McFee

Description: Script to find the conditional probabilities of all amino acids
given the previous amino acid is isoleucine or glutamine
"""

import numpy as np
import pickle

amino_acid_codes = "ANDRCQEGHILKMPFSTWYV"

iso = []
glut = []

for amino in amino_acid_codes:
    iso_key = "I" + amino
    glut_key = "Q" + amino

    iso.append(iso_key)
    glut.append(glut_key)

# Load the di-amino frequencies
file = open("di-amino.pkl", "rb")

diamino = pickle.load(file)

# Don't need this anymore
gt = {
    "A": 0.055,
    "N": 0.044,
    "D": 0.062,
    "R": 0.059,
    "C": 0.013,
    "Q": 0.040,
    "E": 0.066,
    "G": 0.050,
    "H": 0.022,
    "I": 0.065,
    "L": 0.095,
    "K": 0.073,
    "M": 0.021,
    "P": 0.044,
    "F": 0.044,
    "S": 0.090,
    "T": 0.059,
    "W": 0.010,
    "Y": 0.034,
    "V": 0.054
}

amino_file = open("amino.pkl", "rb")
amino_probs = pickle.load(amino_file)

iso_cond = []
glut_cond = []

for item in iso:
    prob = diamino[item] / amino_probs["I"]
    iso_cond.append(prob)

for item in glut:
    prob = diamino[item] / amino_probs["Q"]
    glut_cond.append(prob)

print("isoleucine: ", iso_cond)
print("glutamine: ", glut_cond)

with open("assignment1-q6.txt", "w") as file:
    file.write("isoleucine: " + str(iso_cond) + "\n")
    file.write("glutamine: " + str(glut_cond) + "\n")
