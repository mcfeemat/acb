#!/usr/bin/env python
# coding: utf-8
"""
Author: Matthew C. McFee

Description: Script to find the frequency of di-words in the yeast proteosome
being studied in this assignment. This code uses pattern matching with regular
expressions.
"""

from Bio import SeqIO
import numpy as np
import re
import itertools
import pickle

amino_acid_codes = "ANDRCQEGHILKMPFSTWYV"
amino_acid_codes = list(amino_acid_codes)

diwords = itertools.product(amino_acid_codes, amino_acid_codes)

diword_counts = {}
diword_freqs = {}


def find_count(fasta, diword):
    """
    Find the number of occurences of a diword in all the sequences of a fasta
    file
    """
    total_match = 0

    for record in SeqIO.parse("orf_trans.fasta", "fasta"):
        current_seq = str(record.seq)
        num_match = len(re.findall(diword, current_seq))

        total_match = total_match + num_match

    return total_match


for diword in diwords:
    diword = "".join(diword)
    count = find_count("orf_trans.fasta", diword)

    diword_counts[diword] = count

all_counts = list(diword_counts.values())
all_counts = np.array(all_counts)

total = np.sum(all_counts)

for key, value in diword_counts.items():
    diword_freqs[key] = value / total

output = list(diword_freqs.values())
output = np.array(output).reshape(20, 20)

print(output)

with open("di-amino.pkl", "wb") as file:
    pickle.dump(diword_freqs, file)

with open("assignment1-q5.txt", "w") as file:
    file.write(str(output))
