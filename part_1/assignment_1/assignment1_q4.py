#!/usr/bin/env python
# coding: utf-8
"""
Author: Matthew C. McFee
Description: Script to find the di-word probabilities assuming the amino acids
are independent. For example a di-word could be AG and the output should be a
20x20 probability/frequency matrix.
"""

import numpy as np

# Taken from part 2 of question 2
amino_acid_frequencies = [
    0.05493374, 0.06162652, 0.05851871, 0.04435361, 0.01259785, 0.03954857,
    0.06538423, 0.04974184, 0.02167674, 0.06556859, 0.09500906, 0.07348692,
    0.02080589, 0.04377385, 0.04417267, 0.0898435, 0.05917167, 0.01039371,
    0.03379401, 0.05559833
]

diword_freqs = np.outer(amino_acid_frequencies, amino_acid_frequencies)
print(diword_freqs)

with open("assignment1-q4.txt", "w") as file:
    file.write(str(diword_freqs))
